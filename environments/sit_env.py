ENVIRONMENT = 'SIT'

DB_CONFIG = dict(
    name='infra',
    uri="mongodb://iotadmin:BAIgxRyonjgCnwda@integrationiot-shard-00-00-7pijc.mongodb.net:27017,"
        "integrationiot-shard-00-01-7pijc.mongodb.net:27017,"
        "integrationiot-shard-00-02-7pijc.mongodb.net:27017/infra?ssl=true&streamType=netty&replicaSet=IntegrationIoT"
        "-shard-0&authSource=admin",
    pwd=""  # password included in uri itself
)

MAILER_CONFIG = dict(
    host='smtp.office365.com',
    port=587,
    sender_email='bnadmin@lantrasoft.com',
    pwd='',
    pwd_expiry_date='2021/02/01',  # Y-m-d
    receiver_email=['ajayachathru.s@lantrasoft.com']  # Default admin mail ids
)

TEMETRA_CONFIG = dict(
    kafka_server='54.71.182.242:9092',
    kafka_topic='devicemetrics',
    url='https://temetra.com/',
    username='cmwssb',
    pwd='xxtu495'
)

# first stage
FROM python:3.8-slim

WORKDIR /monitoring

COPY . .

# Install cron
RUN apt-get -y update
RUN apt-get install -y nano
RUN apt-get install -y cron

RUN pip install -r requirements.txt -t /monitoring

RUN chmod +x job.sh

ENTRYPOINT ["/bin/bash","job.sh"]
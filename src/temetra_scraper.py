import csv
import json
import logging
import os
import re
import time
import zipfile
import requests

from datetime import date, datetime, timedelta
from src.utils.config import TEMETRA_CONFIG
from kafka import KafkaProducer


class TemetraScraper:
    time_delta_in_days = 1
    KAFKA_PRODUCER = None
    TEMETRA_BASE_URL = TEMETRA_CONFIG['url']
    TEMETRA_LOGIN_URL = TEMETRA_BASE_URL + "wmsapp/login.do"
    TEMETRA_END_POINT = TEMETRA_BASE_URL + "wmsapp/epoint"

    def __init__(self):
        logging.info("Temetra Scraper class instance created")

    def read_csv_and_publish_to_kafka(self, csv_filename):
        try:
            with open(csv_filename) as csvFile:
                csvReader = csv.DictReader(csvFile)
                for csvRow in csvReader:
                    data = {}
                    subdata = {}
                    data["id"] = str(csvRow["METERSERIAL"])
                    data["rtu"] = str(csvRow["METERSERIAL"])
                    data["data"] = []

                    subdata["dt"] = str(csvRow["TIME"]).replace("T", " ")
                    subdata["TOTF"] = float(csvRow["INDEX"])
                    subdata["INDX"] = float(csvRow["INDEX"])

                    data["data"].append(subdata)
                    try:
                        with open('outputs/temetra_kafka_data.txt', 'a+') as fd:
                            fd.write(str(data) + "\n")
                            logging.info("***Written to file Successfully.")
                            fd.close()
                    except Exception as fwe:
                        logging.error("_____Error in writing to the text file. " + str(fwe))

                    try:
                        future = self.KAFKA_PRODUCER.send(TEMETRA_CONFIG['kafka_topic'], json.dumps(data).encode())
                        result = future.get(timeout=60)
                        logging.info(
                            "***Published KAFKA Message: " + json.dumps(data) + " to Topic: " + TEMETRA_CONFIG[
                                'kafka_topic'])
                    except Exception as kpe:
                        logging.error(
                            "_____Error in publishing message to KAFKA Topic: " + TEMETRA_CONFIG[
                                'kafka_topic'] + " " + str(
                                kpe))

                    # Gap between each iteration
                    time.sleep(1)

        except Exception as rec:
            logging.error("_____Error in reading to the excel file. " + str(rec))

    def download_periodic_report(self, jsessionid):
        """
        1. download yesterday's ipwan zip file
        2. extract zip and get periodic_indices.csv
        3. use periodic_indices.csv to POST data to kafka
        :param jsessionid:
        :return:
        """

        # Get date range
        today_day_start = datetime.combine(date.today(), datetime.min.time())
        yesterday_day_start = today_day_start - timedelta(days=self.time_delta_in_days)

        # URL query parameters
        query_params = {
            "lpwanperiodicdata": "1",
            "from": time.mktime(yesterday_day_start.timetuple()),
            "to": time.mktime(today_day_start.timetuple())
        }

        # Auth cookies
        cookies = {
            "JSESSIONID": jsessionid
        }

        res = requests.get(self.TEMETRA_END_POINT, params=query_params, cookies=cookies)

        zip_file_name = "ipwan_contents.zip"

        if res.status_code == 200:
            try:
                with open(zip_file_name, 'wb').write(res.content):
                    pass  # do nothing

            except Exception as e:
                logging.info("Unable to save {} file".format(zip_file_name) + str(e))

            temp_extracted_folder = 'temp_ipwan_contents'
            # Extract saved zip file
            try:
                with zipfile.ZipFile(zip_file_name, 'r') as zip_ref:
                    zip_ref.extractall(temp_extracted_folder)

                    self.read_csv_and_publish_to_kafka(temp_extracted_folder + '/' + 'periodic_indices.csv')

                    # unlink temp folder
                    os.remove(temp_extracted_folder)

            except Exception as err:
                logging.error("Unable to extract {} file".format(zip_file_name) + str(err))
        else:
            logging.error("Unable to download {} file".format(zip_file_name))

    def logon_to_temetra(self):
        """
        1. log on to temetra using credentials
        2. strip the cookie from callback url
        3. Download ipwan report
        :return:
        """
        request_payload = {
            'userid': TEMETRA_CONFIG['username'],
            'password': TEMETRA_CONFIG['pwd'],
            'apply': 'Log In',
            'datacenters': self.TEMETRA_LOGIN_URL
        }

        login_response = requests.post(self.TEMETRA_LOGIN_URL, data=request_payload)

        if login_response.status_code == 200:
            logging.info("Successfully logged in ...")

            # Extract cookie from callback url
            session = re.findall('jsessionid=([^&]*)', login_response.url)

            if len(session) > 0:
                self.download_periodic_report(session[0])

            else:
                logging.error("Incorrect user id or password !")

        else:
            logging.error("Sorry, unable to login temetra.")

    def run(self, time_delta_in_days=1):
        """
        Initiates step by step process
        1. Logon to temetra
        2. Download ip_wan reports zip file
        3. Extract zip files to folder
        4. read csv file, publish data to BlueNett kakfa
        :return:
        """
        self.time_delta_in_days = int(time_delta_in_days)
        try:
            # Initiate KAFKA Producer
            self.KAFKA_PRODUCER = KafkaProducer(bootstrap_servers=[TEMETRA_CONFIG['kafka_server']], retries=3)

            if self.KAFKA_PRODUCER is not None:
                self.logon_to_temetra()

        except Exception as kce:
            logging.error(
                "____Error in creating producer for KAFKA Server: " + TEMETRA_CONFIG['kafka_server'] + " " + str(kce))

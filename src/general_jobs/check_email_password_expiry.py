import logging
from datetime import datetime

from src.utils.config import MAILER_CONFIG
from src.utils.mailer import send_mail


class SystemMail:
    def __init__(self):
        pass

    def check_password_expiry(self):
        sender_email = MAILER_CONFIG['sender_email']
        today = datetime.today().replace(hour=0, minute=0, second=0)
        pwd_expiry_date = datetime.strptime(MAILER_CONFIG['pwd_expiry_date'], '%Y/%m/%d')
        expiry_in_days = (pwd_expiry_date - today).days

        if expiry_in_days <= 7:
            if expiry_in_days > 0:
                # Password is about to expire
                mail_options = {
                    'subject': "FYI : Account password for {} will expire soon".format(sender_email),
                    'html_template_file': 'src/utils/email_templates/system_mail_password_expiry_template.html',
                    'html_body_params': {
                        'sender_email': sender_email,
                        'days': str(expiry_in_days),
                        'pwd_expiry_date': pwd_expiry_date.strftime("%d %B %Y")
                    }
                }
                send_mail(mail_options)
            else:
                # Password is expired
                logging.error("FYI : Account password for {sender_email} is expired on {pwd_expiry_date}".format(
                    sender_email=sender_email,
                    pwd_expiry_date=pwd_expiry_date.strftime("%d %B %Y")))
        else:
            logging.info("FYI : Account password for {} will expire in {} days".format(sender_email, expiry_in_days))

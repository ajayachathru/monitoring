import datetime as date
import os
import xlsxwriter
import logging

from src.models.networks_model import NetworksModel
from src.utils.mailer import send_mail
from src.utils.util import get_xl_colors

class RtuStatData:
    xl_colors = get_xl_colors()

    def __init__(self):
        logging.info("--------------- RtuStatData class instance created ---------------")

    def create_worksheet(self, data, wb):
        solution = data['_id']
        sheet = wb.add_worksheet(solution)

        sheet.set_column(0, 0, 10)
        sheet.set_column(1, 1, 15)
        sheet.set_column(2, 2, 30)
        sheet.set_column(3, 3, 25)

        # format
        title_format = wb.add_format()
        title_format.font_size = 12
        title_format.set_align('center')
        title_format.set_bg_color(self.xl_colors['header_colors'][0])
        title_format.set_border(1)

        row = 0
        for tenant in data['tenants']:
            sheet.write(row, 0, 'Tenant', title_format)
            sheet.write(row, 1, 'Rtu Id', title_format)
            sheet.write(row, 2, 'Network Name', title_format)
            sheet.write(row, 3, 'Last Received Date', title_format)

            tenant_data = data['tenants'][tenant]
            row += 1
            for d in tenant_data:
                sheet.write(row, 0, tenant)
                sheet.write(row, 1, d['rtu']['_id'])
                sheet.write(row, 2, d['networkName'])
                sheet.write(row, 3, str(d['rtu']['healthStatusLastUpdated'].strftime("%Y-%m-%d %H:%M:%S")))
                row += 1

            # Add extra spacing for next tenant
            row += 2

    def get_rtu_off_data_by_solution_and_tenant(self):
        """
        if we haven't received data for today, which means it's off
        check with healthStatusLastUpdated field in networks collection
        :return:
        """
        today = date.datetime.today()
        today_day_start = today.replace(year=today.year, month=today.month, day=today.day,
                                        hour=0, minute=0, second=0, microsecond=0)

        pipeline = [
            # Stage 1
            {
                '$match': {
                    'solutions': {
                        '$ne': []
                    },
                }
            },

            # Stage 2
            {
                '$unwind': {
                    'path': '$solutions'
                }
            },

            # Stage 3
            {
                '$unwind': {
                    'path': '$rtus'
                }
            },

            # Stage 4
            {
                '$match': {
                    'rtus.healthStatusLastUpdated': {
                        '$lt': today_day_start
                    }
                }
            },

            # Stage 5
            {
                '$sort': {
                    'rtus.healthStatusLastUpdated': -1
                }
            },

            # Stage 6
            {
                '$group': {
                    '_id': {
                        'solution': '$solutions',
                        'tenant': '$tenant',
                    },
                    'data': {
                        '$push': {
                            'rtu': '$rtus',
                            'networkName': '$name'
                        }
                    }
                }
            },

            # Stage 7
            {
                '$group': {
                    '_id': '$_id.solution',
                    'tenants': {
                        '$push': {
                            'k': '$_id.tenant',
                            'v': '$data'
                        }
                    }
                }
            },

            # Stage 8
            {
                '$addFields': {
                    'tenants': {
                        '$arrayToObject': '$tenants'
                    }
                }
            }
        ]

        logging.info('Aggregation pipeline\t' + str(pipeline))

        filename = 'outputs/' + today_day_start.strftime("%d_%m_%Y") + '_rtu_stat.xlsx'
        wb = xlsxwriter.Workbook(filename)
        for solution_wise_data in NetworksModel.objects().aggregate(pipeline, allowDiskUse=True):
            self.create_worksheet(solution_wise_data, wb)

        wb.close()

        # send email
        mail_options = {
            'filepath': filename,
            'filename': today_day_start.strftime("%d_%m_%Y") + '_rtu_stat.xlsx',
            'subject': "BlueNett Monitoring : Daily RTU Health status report - {}".format(
                today_day_start.strftime("%d/%m/%Y")),
            'body': "This is an auto generated email. Please don't reply",
        }
        send_mail(mail_options)

        # Remove file after sending email
        os.remove(filename)

    def daily_rtu_health_monitoring(self):
        """
        Get rtus list for which we have not received data for today
        :return:
        """
        self.get_rtu_off_data_by_solution_and_tenant()

import logging
import os
import xlsxwriter

from src.models.metric_data_h_model import MetricDataHModel
from src.services.device_assets_service import DeviceAssetsService
from src.utils.mailer import send_mail
from src.utils.util import get_monthly_date_range, get_xl_colors

class ItronConsumptionReport:
    device_prop_value = {}  # { "ITRON0001": "20mm" }
    consumption_data = {}
    xl_colors = get_xl_colors()

    def __init__(self):
        pass

    def get_device_ids(self, prop_name):
        device_assets_instance = DeviceAssetsService()
        pipeline = [
            {
                '$match': {
                    "manufRef.name": "Itron"
                }
            }
        ]
        logging.info('Aggregation pipeline\t' + str(pipeline))

        assets = device_assets_instance.get_assets(pipeline)

        device_ids = []
        for device in assets:
            if 'code' in device and (device['code'] not in device_ids):
                device_ids.append(device['code'])

                if 'properties' in device:
                    for prop in device['properties']:
                        if prop['propName'] == prop_name:
                            self.device_prop_value[device['code']] = prop['value']

        return device_ids

    def write_consumption_worksheet(self, workbook, params):

        for prop_value in self.consumption_data:
            sheet = workbook.add_worksheet(prop_value)
            row_num = 0
            # sheet.write(row_num, 0, 'Total 0 Readings')
            # sheet.write(row_num, 1, params['empty_or_wrong_readings']["0"])
            color_indicator_format = workbook.add_format()
            color_indicator_format.set_color('red')
            sheet.write(row_num, 4, 'ABC', color_indicator_format)
            sheet.write(row_num, 5, '0')

            row_num += 1
            # sheet.write(row_num, 0, 'Total NA Readings')
            # sheet.write(row_num, 1, params['empty_or_wrong_readings']["NA"])
            color_indicator_format = workbook.add_format()
            color_indicator_format.set_color('gray')
            sheet.write(row_num, 4, 'ABC', color_indicator_format)
            sheet.write(row_num, 5, '0-5')

            row_num += 1
            # sheet.write(row_num, 0, 'Total >90000 Readings')
            # sheet.write(row_num, 1, params['empty_or_wrong_readings']["90000"])
            color_indicator_format = workbook.add_format()
            color_indicator_format.set_color('green')
            sheet.write(row_num, 4, 'ABC', color_indicator_format)
            sheet.write(row_num, 5, '5-90000')

            row_num += 1
            color_indicator_format = workbook.add_format()
            color_indicator_format.set_color('purple')
            sheet.write(row_num, 4, 'ABC', color_indicator_format)
            sheet.write(row_num, 5, '>90000')

            row_num += 1
            color_indicator_format = workbook.add_format()
            color_indicator_format.set_color('blue')
            sheet.write(row_num, 4, 'ABC', color_indicator_format)
            sheet.write(row_num, 5, 'NA')

            row_num += 1
            sheet.set_column(0, 0, 20)
            # sheet.set_column(1, 1, 20)
            # sheet.set_column(2, 2, 20)
            sheet.merge_range(row_num, 0, row_num + 1, 0, 'Meter ID', params['title_format'])
            # sheet.merge_range(row_num, 1, row_num + 1, 1, 'Depo', params['title_format'])
            # sheet.merge_range(row_num, 2, row_num + 1, 2, 'Installed Date', params['title_format'])

            # Bind Headers -------------------------------
            idx = 0
            col = 1
            for date_range in params['date_range_input']:
                if idx > 0:
                    cell_format = workbook.add_format()
                    cell_format.font_size = 12
                    cell_format.set_align('center')
                    cell_format.set_bg_color(self.xl_colors['header_colors'][idx % 2])
                    cell_format.set_border(1)

                    sheet.merge_range(row_num, col, row_num, col + 4, date_range['display_name'] + ' Month',
                                      cell_format)

                    sheet.set_column(col, col, 20)
                    sheet.write(row_num + 1, col, 'Last Read Date', cell_format)

                    col += 1
                    sheet.set_column(col, col, 20)
                    sheet.write(row_num + 1, col, 'Index', cell_format)

                    col += 1
                    sheet.set_column(col, col, 20)
                    sheet.write(row_num + 1, col, 'Previous Read Date', cell_format)

                    col += 1
                    sheet.set_column(col, col, 20)
                    sheet.write(row_num + 1, col, 'Index', cell_format)

                    col += 1
                    sheet.set_column(col, col, 20)
                    sheet.write(row_num + 1, col, 'Total Consumption', cell_format)

                    # Next start
                    col += 1
                idx += 1
            # Binding Headers completed -------------------------------
            # Bind Values ---------------------------------
            row_num += 2

            monthly_total = {}
            monthly_qty = {}

            for device in self.consumption_data[prop_value]:
                sheet.write(row_num, 0, device)
                # sheet.write(row_num, 1, self.consumption_data[device]['orgunit'])
                # sheet.write(row_num, 2, "")

                idx = 0
                col = 1
                for date_range in params['date_range_input']:
                    if idx > 0:
                        d = self.consumption_data[prop_value][device]['monthly'][date_range['key']]

                        cell_format = workbook.add_format()
                        cell_format.set_color(d['cell_color'])
                        cell_format.set_align('right')

                        sheet.write(row_num, col, d['current_month_read_date'], cell_format)
                        sheet.write(row_num, col + 1, d['current_month_index'], cell_format)
                        sheet.write(row_num, col + 2, d['previous_month_read_date'], cell_format)
                        sheet.write(row_num, col + 3, d['previous_month_index'], cell_format)
                        sheet.write(row_num, col + 4, d['total_consumption'], cell_format)

                        if date_range['key'] not in monthly_total:
                            monthly_total[date_range['key']] = 0
                            monthly_qty[date_range['key']] = 0

                        if d['total_consumption'] != 'NA' and float(d['total_consumption']) > 0:
                            monthly_total[date_range['key']] += float(d['total_consumption'])

                        if d['total_consumption'] != 'NA' and 0 < float(d['total_consumption']) < 100000:
                            monthly_qty[date_range['key']] += 1

                        # Next start
                        col = col + 5
                    idx += 1

                row_num += 1

            col = 1
            row_num += 1

            idx = 0
            for date_range in params['date_range_input']:
                if idx > 0:
                    sheet.write(row_num, col + 3, 'TOTAL')
                    sheet.write(row_num, col + 4, monthly_total[date_range['key']].__round__(2))
                    sheet.write(row_num + 1, col + 3, 'Qty (0 - 100000)')
                    sheet.write(row_num + 1, col + 4, monthly_qty[date_range['key']])

                    # Next start
                    col = col + 5
                idx += 1

    def generate_excel(self, date_range_input, empty_or_wrong_readings):
        report_from = date_range_input[1]['from']
        report_to = date_range_input[len(date_range_input) - 1]['to']

        formatted_from_date = report_from.strftime("%Y-%m-%d")
        formatted_to_date = report_to.strftime("%Y-%m-%d")

        filename = "outputs/itron_{}_{}.xlsx".format(formatted_from_date, formatted_to_date)
        workbook = xlsxwriter.Workbook(filename)

        # format
        title_format = workbook.add_format()
        title_format.font_size = 12
        title_format.set_align('center')
        title_format.set_bg_color(self.xl_colors['header_colors'][0])
        title_format.set_border(1)

        params = dict(
            empty_or_wrong_readings=empty_or_wrong_readings,
            date_range_input=date_range_input,
            formatted_from_date=formatted_from_date,
            formatted_to_date=formatted_to_date,
            title_format=title_format
        )

        self.write_consumption_worksheet(workbook, params)

        workbook.close()

        # send email
        mail_options = {
            'filepath': filename,
            'filename': "itron_{}_{}.xlsx".format(formatted_from_date, formatted_to_date),
            'subject': "BlueNett Monitoring : Monthly Itron Report {} to {}".format(formatted_from_date, formatted_to_date),
            'body': "This is an auto generated email. Please don't reply",
        }
        send_mail(mail_options)

        # Remove file after sending email
        os.remove(filename)

    def monthly_itron_consumption_report(self, prop_name='diameter'):
        device_ids = self.get_device_ids(prop_name)

        # Pass Year, Month, Date
        date_range_input = get_monthly_date_range()

        range_from = date_range_input[0]['from']
        range_to = date_range_input[len(date_range_input) - 1]['to']

        monthly_facet = {}
        monthly_facet_keys = []

        for obj in date_range_input:
            key = obj['key']
            monthly_facet_keys.append('$' + key)
            monthly_facet[key] = [
                {
                    '$project': {
                        'data': {
                            '$filter': {
                                'input': '$data',
                                'as': 'data',
                                'cond': {
                                    '$and': [
                                        {
                                            '$gte': [
                                                '$$data.dated', obj['from']
                                            ]
                                        }, {
                                            '$lte': [
                                                '$$data.dated', obj['to']
                                            ]
                                        }
                                    ]
                                }
                            }
                        }
                    }
                },
                {
                    '$project': {
                        '_id': 1,
                        'key': key,
                        'data': {
                            '$arrayElemAt': [
                                '$data', -1
                            ]
                        }
                    }
                }
            ]

        # Create Aggregation pipeline to filter records
        pipeline = [
            {
                '$match': {
                    # 'network': 50,
                    'metric': 'INDX',
                    'deviceId': {
                        '$in': device_ids
                    },
                    'dated': {
                        '$gte': range_from,
                        '$lte': range_to
                    }
                }
            },
            {
                '$group': {
                    '_id': '$deviceId',
                    'network': {
                        '$first': '$network'
                    },
                    'data': {
                        '$push': {
                            'dated': '$dated',
                            'index': {
                                '$arrayElemAt': [
                                    '$data', 0
                                ]
                            }
                        }
                    }
                }
            },
            {
                '$lookup': {
                    'from': 'orgunits',
                    'localField': 'network',
                    'foreignField': 'networks.refId',
                    'as': 'orgunit'
                }
            },
            {
                '$addFields': {
                    'orgunit': {
                        '$arrayElemAt': [
                            '$orgunit', 0
                        ]
                    }
                }
            },
            {
                '$addFields': {
                    'orgunit': '$orgunit.name'
                }
            },
            {
                '$facet': monthly_facet
            },
            {
                '$project': {
                    'summary': {
                        '$concatArrays': monthly_facet_keys
                    }
                }
            },
            {
                '$unwind': {
                    'path': '$summary'
                }
            },
            {
                '$group': {
                    '_id': '$summary._id',
                    'orgunit': {
                        '$first': '$summary.orgunit'
                    },
                    'monthly': {
                        '$push': {
                            'key': '$summary.key',
                            'dated': {
                                '$dateToString': {
                                    'format': "%Y-%m-%d %H:%M:%S",
                                    'date': '$summary.data.dated'
                                }
                            },
                            'index': '$summary.data.index'
                        }
                    }
                }
            }
        ]
        logging.info('Aggregation pipeline\t' + str(pipeline))

        empty_or_wrong_readings = {
            "0": 0,
            "NA": 0,
            "90000": 0,
        }

        device_data = {}
        for metric_data_h in MetricDataHModel.objects().aggregate(pipeline, allowDiskUse=True):
            meter_id = metric_data_h['_id']
            orgunit = metric_data_h['orgunit']

            if meter_id not in device_data:
                device_data[meter_id] = {}
                device_data[meter_id]['orgunit'] = orgunit
                device_data[meter_id]['monthly'] = {}

                if 'monthly' in metric_data_h:

                    monthly_data = {}
                    for month in metric_data_h['monthly']:
                        monthly_data[month['key']] = month

                    # Flags to identify count
                    all_zero = True
                    all_not_available = True
                    all_greater_than_90000 = True

                    idx = 0
                    for date_range in date_range_input:
                        if idx > 0:
                            current_month = date_range_input[idx]['key']
                            previous_month = date_range_input[idx - 1]['key']

                            if monthly_data[current_month]['dated'] is None:
                                monthly_data[current_month] = {
                                    'dated': 'NA',
                                    'index': 'NA',
                                }

                            if monthly_data[previous_month]['dated'] is None:
                                monthly_data[previous_month] = {
                                    'dated': 'NA',
                                    'index': 'NA',
                                }

                            device_data[meter_id]['monthly'][current_month] = {
                                'previous_month_read_date': monthly_data[previous_month]['dated'],
                                'previous_month_index': monthly_data[previous_month]['index'],
                                'current_month_read_date': monthly_data[current_month]['dated'],
                                'current_month_index': monthly_data[current_month]['index'],

                                # default values, will be modified further
                                'cell_color': 'blue',
                                'total_consumption': 'NA'
                            }

                            if monthly_data[current_month]['index'] != 'NA' and monthly_data[previous_month][
                                'index'] != 'NA':

                                total_consumption = monthly_data[current_month]['index'] - monthly_data[previous_month][
                                    'index']

                                device_data[meter_id]['monthly'][current_month][
                                    'total_consumption'] = total_consumption

                                cell_color = 'red'
                                if 0 < total_consumption <= 5:
                                    cell_color = 'gray'
                                elif 5 < total_consumption <= 90000:
                                    cell_color = 'green'
                                elif total_consumption > 90000:
                                    cell_color = 'purple'

                                device_data[meter_id]['monthly'][current_month]['cell_color'] = cell_color

                            # Check current consumption qty
                            current_month_consumption = float(monthly_data[current_month]['index'])
                            if current_month_consumption == 'NA':

                                all_greater_than_90000 = False
                                all_zero = False

                            elif current_month_consumption <= 0:

                                all_greater_than_90000 = False
                                all_not_available = False

                            elif 0 < current_month_consumption <= 5:

                                all_zero = False
                                all_greater_than_90000 = False
                                all_not_available = False

                            elif 5 < current_month_consumption <= 90000:

                                all_zero = False
                                all_greater_than_90000 = False
                                all_not_available = False

                            elif current_month_consumption > 90000:

                                all_zero = False
                                all_not_available = False
                        idx += 1

                    if all_zero:
                        empty_or_wrong_readings["0"] += 1

                    if all_greater_than_90000:
                        empty_or_wrong_readings["90000"] += 1

                    if all_not_available:
                        empty_or_wrong_readings["NA"] += 1

        for device in device_ids:
            if device not in device_data:
                empty_or_wrong_readings["NA"] += 1

                device_data[device] = {}
                device_data[device]['orgunit'] = ''
                device_data[device]['monthly'] = {}

                idx = 0
                for date_range in date_range_input:
                    if idx > 0:
                        device_data[device]['monthly'][date_range['key']] = {
                            'previous_month_read_date': 'NA',
                            'previous_month_index': 'NA',
                            'current_month_read_date': 'NA',
                            'current_month_index': 'NA',
                            'cell_color': 'blue',
                            'total_consumption': 'NA'
                        }
                    idx += 1

        for d in device_data:
            prop_value = self.device_prop_value[d]

            if prop_value not in self.consumption_data:
                self.consumption_data[prop_value] = {}

            self.consumption_data[prop_value][d] = device_data[d]

        self.generate_excel(date_range_input, empty_or_wrong_readings)

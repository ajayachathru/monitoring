import os
import xlsxwriter
import logging

from src.models.metric_data_h_model import MetricDataHModel
from src.services.netnodes_service import NetNodesService
from src.utils.log import log_completion_time
from src.utils.mailer import send_mail
from src.utils.util import get_monthly_date_range, get_xl_colors


class AmrReport:
    depo_data = {}
    consumption_data = {}
    xl_colors = get_xl_colors()

    def __init__(self):
        pass

    @log_completion_time
    def get_device_ids(self):
        net_nodes_instance = NetNodesService()
        pipeline = [
            {
                '$match': {
                    "nodeType": 'AMR',
                    "tenant": "CMWSSB"
                }
            }
        ]
        logging.info('Aggregation pipeline\t' + str(pipeline))
        net_nodes = net_nodes_instance.get_net_nodes(pipeline)

        device_ids = []
        for net_node in net_nodes:
            for device in net_node["devices"]:
                # Collect unique ids
                if '_id' in device and (device['_id'] not in device_ids):
                    device_ids.append(device['_id'])
        return device_ids

    def write_depo_worksheet(self, workbook, params):
        sheet = workbook.add_worksheet('Depo_report')

        row_num = 0
        sheet.merge_range(row_num, 0, row_num, 5, 'AMR ANALYTICAL REPORT', params['title_format'])
        sheet.set_column(0, 0, 20)
        sheet.set_column(1, 1, 20)

        row_num += 2
        sheet.write(row_num, 0, 'From & To Date')
        sheet.write(row_num, 1, "{} to {}".format(params['formatted_from_date'], params['formatted_to_date']))

        row_num += 2

        sheet.merge_range(row_num, 0, row_num + 1, 0, 'Depo', params['title_format'])
        # Bind Headers -------------------------------
        idx = 0
        col = 1
        for date_range in params['date_range_input']:
            if idx > 0:
                cell_format = workbook.add_format()
                cell_format.font_size = 12
                cell_format.set_align('center')
                cell_format.set_bg_color(self.xl_colors['header_colors'][idx % 2])
                cell_format.set_border(1)

                sheet.merge_range(row_num, col, row_num, col + 3, date_range['display_name'] + ' Month', cell_format)

                sheet.set_column(col, col, 20)
                sheet.write(row_num + 1, col, '0', cell_format)

                col += 1
                sheet.set_column(col, col, 20)
                sheet.write(row_num + 1, col, '0-5', cell_format)

                col += 1
                sheet.set_column(col, col, 20)
                sheet.write(row_num + 1, col, '5-90000', cell_format)

                col += 1
                sheet.set_column(col, col, 20)
                sheet.write(row_num + 1, col, '>90000', cell_format)

                # Next start
                col += 1
            idx += 1
        # Binding Headers completed -------------------------------

        # Bind Values ---------------------------------
        row_num += 2
        for org in self.depo_data:
            sheet.write(row_num, 0, org)

            idx = 0
            col = 1
            for date_range in params['date_range_input']:
                if idx > 0:
                    d = self.depo_data[org][date_range['key']]

                    cell_format = workbook.add_format()
                    cell_format.set_align('right')

                    sheet.write(row_num, col, d['0'], cell_format)
                    sheet.write(row_num, col + 1, d['0_5'], cell_format)
                    sheet.write(row_num, col + 2, d['5_90000'], cell_format)
                    sheet.write(row_num, col + 3, d['90000'], cell_format)

                    # Next start
                    col = col + 4
                idx += 1

            row_num += 1

    def write_consumption_worksheet(self, workbook, params):
        sheet = workbook.add_worksheet('Consumption_Report')

        row_num = 0
        sheet.write(row_num, 0, 'Total 0 Readings')
        sheet.write(row_num, 1, params['empty_or_wrong_readings']["0"])
        color_indicator_format = workbook.add_format()
        color_indicator_format.set_color('red')
        sheet.write(row_num, 4, 'ABC', color_indicator_format)
        sheet.write(row_num, 5, '0')

        row_num += 1
        sheet.write(row_num, 0, 'Total NA Readings')
        sheet.write(row_num, 1, params['empty_or_wrong_readings']["NA"])
        color_indicator_format = workbook.add_format()
        color_indicator_format.set_color('gray')
        sheet.write(row_num, 4, 'ABC', color_indicator_format)
        sheet.write(row_num, 5, '0-5')

        row_num += 1
        sheet.write(row_num, 0, 'Total >90000 Readings')
        sheet.write(row_num, 1, params['empty_or_wrong_readings']["90000"])
        color_indicator_format = workbook.add_format()
        color_indicator_format.set_color('green')
        sheet.write(row_num, 4, 'ABC', color_indicator_format)
        sheet.write(row_num, 5, '5-90000')

        row_num += 1
        color_indicator_format = workbook.add_format()
        color_indicator_format.set_color('purple')
        sheet.write(row_num, 4, 'ABC', color_indicator_format)
        sheet.write(row_num, 5, '>90000')

        row_num += 1
        color_indicator_format = workbook.add_format()
        color_indicator_format.set_color('blue')
        sheet.write(row_num, 4, 'ABC', color_indicator_format)
        sheet.write(row_num, 5, 'NA')

        row_num += 1
        sheet.set_column(0, 0, 20)
        sheet.set_column(1, 1, 20)
        sheet.set_column(2, 2, 20)
        sheet.merge_range(row_num, 0, row_num + 1, 0, 'Meter ID', params['title_format'])
        sheet.merge_range(row_num, 1, row_num + 1, 1, 'Depo', params['title_format'])
        sheet.merge_range(row_num, 2, row_num + 1, 2, 'Installed Date', params['title_format'])

        # Bind Headers -------------------------------
        idx = 0
        col = 3
        for date_range in params['date_range_input']:
            if idx > 0:
                cell_format = workbook.add_format()
                cell_format.font_size = 12
                cell_format.set_align('center')
                cell_format.set_bg_color(self.xl_colors['header_colors'][idx % 2])
                cell_format.set_border(1)

                sheet.merge_range(row_num, col, row_num, col + 4, date_range['display_name'] + ' Month',
                                  cell_format)

                sheet.set_column(col, col, 20)
                sheet.write(row_num + 1, col, 'Last Read Date', cell_format)

                col += 1
                sheet.set_column(col, col, 20)
                sheet.write(row_num + 1, col, 'Index', cell_format)

                col += 1
                sheet.set_column(col, col, 20)
                sheet.write(row_num + 1, col, 'Previous Read Date', cell_format)

                col += 1
                sheet.set_column(col, col, 20)
                sheet.write(row_num + 1, col, 'Index', cell_format)

                col += 1
                sheet.set_column(col, col, 20)
                sheet.write(row_num + 1, col, 'Total Consumption', cell_format)

                # Next start
                col += 1
            idx += 1
        # Binding Headers completed -------------------------------

        # Bind Values ---------------------------------
        row_num += 2
        for device in self.consumption_data:
            sheet.write(row_num, 0, device)
            sheet.write(row_num, 1, self.consumption_data[device]['orgunit'])
            sheet.write(row_num, 2, "")

            idx = 0
            col = 3
            for date_range in params['date_range_input']:
                if idx > 0:
                    d = self.consumption_data[device]['monthly'][date_range['key']]

                    cell_format = workbook.add_format()
                    cell_format.set_color(d['cell_color'])
                    cell_format.set_align('right')

                    sheet.write(row_num, col, d['current_month_read_date'], cell_format)
                    sheet.write(row_num, col + 1, d['current_month_index'], cell_format)
                    sheet.write(row_num, col + 2, d['previous_month_read_date'], cell_format)
                    sheet.write(row_num, col + 3, d['previous_month_index'], cell_format)
                    sheet.write(row_num, col + 4, d['total_consumption'], cell_format)

                    # Next start
                    col = col + 5
                idx += 1

            row_num += 1

    def generate_excel(self, date_range_input, empty_or_wrong_readings):
        report_from = date_range_input[1]['from']
        report_to = date_range_input[len(date_range_input) - 1]['to']

        formatted_from_date = report_from.strftime("%Y-%m-%d")
        formatted_to_date = report_to.strftime("%Y-%m-%d")

        filename = "outputs/AMR_{}_{}.xlsx".format(formatted_from_date, formatted_to_date)
        workbook = xlsxwriter.Workbook(filename)

        # format
        title_format = workbook.add_format()
        title_format.font_size = 12
        title_format.set_align('center')
        title_format.set_bg_color('#B4C6E7')
        title_format.set_border(1)

        params = dict(
            empty_or_wrong_readings=empty_or_wrong_readings,
            date_range_input=date_range_input,
            formatted_from_date=formatted_from_date,
            formatted_to_date=formatted_to_date,
            title_format=title_format
        )

        self.write_depo_worksheet(workbook, params)

        self.write_consumption_worksheet(workbook, params)

        workbook.close()

        # send email
        mail_options = {
            'filepath': filename,
            'filename': "AMR_{}_{}.xlsx".format(formatted_from_date, formatted_to_date),
            'subject': "BlueNett Monitoring : Monthly AMR Report {} to {}".format(formatted_from_date,
                                                                                  formatted_to_date),
            'body': "This is an auto generated email. Please don't reply",
        }
        send_mail(mail_options)

        # Remove file after sending email
        os.remove(filename)

    @log_completion_time
    def monthly_amr_consumption_report(self):
        device_ids = self.get_device_ids()

        # Pass Year, Month, Date
        date_range_input = get_monthly_date_range()

        range_from = date_range_input[0]['from']
        range_to = date_range_input[len(date_range_input) - 1]['to']

        monthly_facet = {}
        monthly_facet_keys = []

        for obj in date_range_input:
            key = obj['key']
            monthly_facet_keys.append('$' + key)
            monthly_facet[key] = [
                {
                    '$project': {
                        'orgunit': 1,
                        'data': {
                            '$filter': {
                                'input': '$data',
                                'as': 'data',
                                'cond': {
                                    '$and': [
                                        {
                                            '$gte': [
                                                '$$data.dated', obj['from']
                                            ]
                                        }, {
                                            '$lte': [
                                                '$$data.dated', obj['to']
                                            ]
                                        }
                                    ]
                                }
                            }
                        }
                    }
                },
                {
                    '$project': {
                        '_id': 1,
                        'orgunit': 1,
                        'key': key,
                        'data': {
                            '$arrayElemAt': [
                                '$data', -1
                            ]
                        }
                    }
                }
            ]

        # Create Aggregation pipeline to filter records
        pipeline = [
            {
                '$match': {
                    # 'network': 50,
                    'metric': 'INDX',
                    'deviceId': {
                        '$in': device_ids
                    },
                    'dated': {
                        '$gte': range_from,
                        '$lte': range_to
                    }
                }
            },
            {
                '$group': {
                    '_id': '$deviceId',
                    'network': {
                        '$first': '$network'
                    },
                    'data': {
                        '$push': {
                            'dated': '$dated',
                            'index': {
                                '$arrayElemAt': [
                                    '$data', 0
                                ]
                            }
                        }
                    }
                }
            },
            {
                '$lookup': {
                    'from': 'orgunits',
                    'localField': 'network',
                    'foreignField': 'networks.refId',
                    'as': 'orgunit'
                }
            },
            {
                '$addFields': {
                    'orgunit': {
                        '$arrayElemAt': [
                            '$orgunit', 0
                        ]
                    }
                }
            },
            {
                '$addFields': {
                    'orgunit': '$orgunit.name'
                }
            },
            {
                '$facet': monthly_facet
            },
            {
                '$project': {
                    'summary': {
                        '$concatArrays': monthly_facet_keys
                    }
                }
            },
            {
                '$unwind': {
                    'path': '$summary'
                }
            },
            {
                '$group': {
                    '_id': '$summary._id',
                    'orgunit': {
                        '$first': '$summary.orgunit'
                    },
                    'monthly': {
                        '$push': {
                            'key': '$summary.key',
                            'dated': {
                                '$dateToString': {
                                    'format': "%Y-%m-%d %H:%M:%S",
                                    'date': '$summary.data.dated'
                                }
                            },
                            'index': '$summary.data.index'
                        }
                    }
                }
            }
        ]
        logging.info('Aggregation pipeline\t' + str(pipeline))
        empty_or_wrong_readings = {
            "0": 0,
            "NA": 0,
            "90000": 0,
        }

        for metric_data_h in MetricDataHModel.objects().aggregate(pipeline, allowDiskUse=True):
            meter_id = metric_data_h['_id']
            orgunit = metric_data_h['orgunit']

            if meter_id not in self.consumption_data:
                self.consumption_data[meter_id] = {}
                self.consumption_data[meter_id]['orgunit'] = orgunit
                self.consumption_data[meter_id]['monthly'] = {}

                if orgunit not in self.depo_data:
                    self.depo_data[orgunit] = {}

                if 'monthly' in metric_data_h:

                    monthly_data = {}
                    for month in metric_data_h['monthly']:
                        monthly_data[month['key']] = month

                        if month['key'] not in self.depo_data[orgunit]:
                            self.depo_data[orgunit][month['key']] = {
                                "0": 0,
                                "0_5": 0,
                                "5_90000": 0,
                                "90000": 0,
                            }

                    # Flags to identify count
                    all_zero = True
                    all_not_available = True
                    all_greater_than_90000 = True

                    idx = 0
                    for _ in date_range_input:
                        if idx > 0:
                            current_month = date_range_input[idx]['key']
                            previous_month = date_range_input[idx - 1]['key']

                            if monthly_data[current_month]['dated'] is None:
                                monthly_data[current_month] = {
                                    'dated': 'NA',
                                    'index': 'NA',
                                }

                            if monthly_data[previous_month]['dated'] is None:
                                monthly_data[previous_month] = {
                                    'dated': 'NA',
                                    'index': 'NA',
                                }

                            self.consumption_data[meter_id]['monthly'][current_month] = {
                                'previous_month_read_date': monthly_data[previous_month]['dated'],
                                'previous_month_index': monthly_data[previous_month]['index'],
                                'current_month_read_date': monthly_data[current_month]['dated'],
                                'current_month_index': monthly_data[current_month]['index'],
                                'cell_color': 'blue',  # default values, will be modified further
                                'total_consumption': 'NA'
                            }

                            if monthly_data[current_month]['index'] != 'NA' and monthly_data[previous_month]['index'] != 'NA':

                                total_consumption = monthly_data[current_month]['index'] - monthly_data[previous_month][
                                    'index']

                                self.consumption_data[meter_id]['monthly'][current_month][
                                    'total_consumption'] = total_consumption

                                cell_color = 'red'
                                if 0 < total_consumption <= 5:
                                    cell_color = 'gray'
                                elif 5 < total_consumption <= 90000:
                                    cell_color = 'green'
                                elif total_consumption > 90000:
                                    cell_color = 'purple'

                                self.consumption_data[meter_id]['monthly'][current_month]['cell_color'] = cell_color

                            # Check current consumption qty
                            current_month_consumption = float(monthly_data[current_month]['index'])
                            if current_month_consumption == 'NA':
                                self.depo_data[orgunit][current_month]["0"] += 1
                                all_greater_than_90000 = False
                                all_zero = False

                            elif current_month_consumption <= 0:
                                self.depo_data[orgunit][current_month]["0"] += 1
                                all_greater_than_90000 = False
                                all_not_available = False

                            elif 0 < current_month_consumption <= 5:
                                self.depo_data[orgunit][current_month]["0_5"] += 1
                                all_zero = False
                                all_greater_than_90000 = False
                                all_not_available = False

                            elif 5 < current_month_consumption <= 90000:
                                self.depo_data[orgunit][current_month]["5_90000"] += 1
                                all_zero = False
                                all_greater_than_90000 = False
                                all_not_available = False

                            elif current_month_consumption > 90000:
                                self.depo_data[orgunit][current_month]["90000"] += 1
                                all_zero = False
                                all_not_available = False
                        idx += 1

                    if all_zero:
                        empty_or_wrong_readings["0"] += 1

                    if all_greater_than_90000:
                        empty_or_wrong_readings["90000"] += 1

                    if all_not_available:
                        empty_or_wrong_readings["NA"] += 1

        for device in device_ids:
            if device not in self.consumption_data:
                empty_or_wrong_readings["NA"] += 1

                self.consumption_data[device] = {}
                self.consumption_data[device]['orgunit'] = ''
                self.consumption_data[device]['monthly'] = {}

                idx = 0
                for date_range in date_range_input:
                    if idx > 0:
                        self.consumption_data[device]['monthly'][date_range['key']] = {
                            'previous_month_read_date': 'NA',
                            'previous_month_index': 'NA',
                            'current_month_read_date': 'NA',
                            'current_month_index': 'NA',
                            'cell_color': 'blue',
                            'total_consumption': 'NA'
                        }
                    idx += 1

        self.generate_excel(date_range_input, empty_or_wrong_readings)

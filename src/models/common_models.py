from mongoengine import *


class Address(EmbeddedDocument):
    addressLine1: StringField()
    city: StringField()
    state: StringField()
    zipCode: StringField()


class GeoLocation(EmbeddedDocument):
    x: IntField(default=0)
    y: IntField(default=0)
    coordinates: ListField(default=0)
    type: StringField(default="Point")


class Properties(EmbeddedDocument):
    propName: StringField(required=True)
    propDataType: StringField(required=True)
    dispLabel: StringField(required=True)
    isRequired: BooleanField(default=False)
    isCommand: BooleanField(default=False)
    value: StringField(required=True)


class NetNodeMetrics(EmbeddedDocument):
    metricType: StringField(required=True)
    code: StringField(required=True)
    name: StringField(required=True)
    dataType: StringField(required=True)
    factor: IntField(required=True)
    unit: StringField(required=True)
    displayUnit: StringField(required=True)
    deviceUnit: StringField(required=True)


class NetNodeDeviceType(EmbeddedDocument):
    refId: IntField(required=True)
    name: StringField(required=True)


class NetNodeDevices(EmbeddedDocument):
    _id: StringField(required=True)
    rtuCode: StringField(required=True)
    measuresPerHour: IntField(default=0)
    metrics: ListField(
        EmbeddedDocumentField(NetNodeMetrics)
    )
    deviceType: EmbeddedDocumentField(NetNodeDeviceType)
    assetRef: IntField(required=True)
    model: StringField(required=True)
    version: StringField(required=True)
    tenant: StringField(required=True)
    active: BooleanField(default=True)

from mongoengine import *


class NetworksModel(DynamicDocument):
    # Collection Reference..
    meta = {
        'collection': 'networks'
    }

    _id: IntField(required=True)
    rtus: ListField(required=True)
    templateName: StringField()
    solutions: ListField(required=True)
    networkAccess: ListField()
    networkGroups: ListField()
    name: StringField(required=True)
    code: StringField(required=True)
    descript: StringField()
    tenant: StringField(required=True)
    properties: ListField()
    active: BooleanField(default=True)
    _class: StringField()
    timezone: StringField(required=True)


from mongoengine import *

from src.models.common_models import Address, GeoLocation,\
    Properties, NetNodeDevices, NetNodeMetrics


class NetNodesModel(DynamicDocument):
    # Collection Reference
    meta = {
        'collection': 'netnodes',
        'indexes': [
            'nodeType',
            'tenant'
        ]
    }

    _id: IntField()
    nodeType: StringField(required=True)
    solution: StringField(required=True)
    address: EmbeddedDocumentField(Address)
    location: EmbeddedDocumentField(GeoLocation)
    devices: EmbeddedDocumentField(NetNodeDevices)
    metrics: EmbeddedDocumentField(NetNodeMetrics)
    name: StringField(required=True)
    code: StringField(required=True)
    descript: StringField(required=True)
    tenant: StringField(required=True)
    properties: EmbeddedDocumentField(Properties)
    active: BooleanField(default=True)
    _class: StringField()

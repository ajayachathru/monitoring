from mongoengine import *


class EventsModel(DynamicDocument):
    # Collection Reference
    meta = {
        'collection': 'events'
    }

    _id: StringField()
    networkId: IntField(required=True)
    nodeId: IntField(required=True)
    deviceId: StringField(required=True)

    nodeType: StringField(required=True)

    rtu: StringField(required=True)
    assetRef: IntField(required=True)
    tenant: StringField(required=True)
    solution: StringField(required=True)
    eventTime: DateField(required=True)
    receiveTime: DateField(required=True)
    metricCode: StringField(required=True)
    metricName: StringField(required=True)
    metricDataType: StringField(required=True)
    metricAggType: StringField(required=True)
    metricUnit: StringField(required=True)
    value: IntField(required=True)
    timezone: StringField(required=True)
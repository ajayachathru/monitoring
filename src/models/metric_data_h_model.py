from mongoengine import *


class MetricDataHModel(DynamicDocument):
    # Collection Reference
    meta = {
        'collection': 'metric_data_h'
    }

    _id: IntField()
    dated: DateField(required=True)
    ymd: StringField(required=True)
    network: IntField()
    nodeId: IntField()
    deviceId: StringField(required=True)
    metric: StringField(required=True)
    aggregationType: StringField(required=True)
    data: ListField()
    _class: StringField()
from mongoengine import *


class RtuStatDataModel(DynamicDocument):
    # Collection Reference..
    meta = {
        'collection': 'rtu_stat_data'
    }

    _id: ObjectIdField()
    rtuId: StringField(required=True)
    network: IntField(required=True)
    tenant: StringField(required=True)
    dated: DateField(required=True)
    stat: DictField(required=True)
    _class: StringField(default='')

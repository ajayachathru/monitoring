from mongoengine import *


class DeviceAssetsModel(DynamicDocument):
    # Collection Reference
    meta = {
        'collection': 'device_assets'
    }

    _id: IntField()
    code: StringField(required=True)
    tagNbr: StringField(required=True)
    serialNbr: StringField(required=True)
    macNbr: StringField(required=True)
    type: DictField(required=True)
    status: StringField(required=True)
    manufRef: DictField(required=True)
    categoryRef: DictField(required=True)
    manufDate: DateField(required=True)
    inventoryDate: DateField(required=True)
    installDate: DateField(required=True)
    tenantCd: StringField(required=True)
    transFreq: IntField(required=True)
    name: StringField(required=True)
    description: StringField(default='')
    properties: DictField(default={})
    active: BooleanField(default=True)
    _class: StringField(default='')
    commissionDate: DateField(required=True)
import logging
from datetime import timedelta
from time import time

# ------------------------- Function wrappers ! -------------------------
def log_completion_time(func):
    """
    To log time taken to complete the function
    usage: annotate this above the function
    :param func:
    :return:
    """
    def f(*args, **kwargs):
        logging.info('================ {.__name__}() started ================'.format(func))
        time_before_prcoess = time()

        rv = func(*args, **kwargs)

        time_after_prcoess = time()
        time_taken = timedelta(seconds=time_after_prcoess - time_before_prcoess)

        logging.info('================ {.__name__}() completed, '.format(func)
              + f'Time taken = {str(time_taken)} ================ \n')
        return rv
    return f

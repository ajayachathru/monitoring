import datetime as date


def get_xl_colors():
    colors = {
        'header_colors': ['#B4C6E7', '#E4DFEC']
    }
    return colors

def get_monthly_date_range(include_extra_prev_month=True):
    """
    :param include_extra_prev_month: adds one more previous month
    :return:
    [
        {
            'key': 'Oct_2020',
            'display_name': 'October',
            'from': date.datetime(2020, 10, 1, 0, 0, 0),
            'to': date.datetime(2020, 10, 31, 23, 59, 59),
        },
        {
            'key': 'Nov_2020',
            'display_name': 'November',
            'from': date.datetime(2020, 11, 1, 0, 0, 0),
            'to': date.datetime(2020, 11, 30, 23, 59, 59),
        },
    ]
    """
    date_range = []

    today = date.datetime.today()
    current_month_day_start = today.replace(day=1)

    number_of_months_to_generate_report = 1

    if include_extra_prev_month:
        # By default +1 because we need to show previous month data in current month
        number_of_months_to_generate_report += 1

    for x in range(number_of_months_to_generate_report):
        previous_month_day_end = current_month_day_start - date.timedelta(days=1)
        previous_month_day_start = previous_month_day_end.replace(day=1)

        previous_month_day_start = previous_month_day_start.replace(hour=0, minute=0, second=0)
        previous_month_day_end = previous_month_day_end.replace(hour=23, minute=59, second=59)

        obj = {
            'key': previous_month_day_start.strftime('%b_%Y'),
            'display_name': previous_month_day_start.strftime('%B'),
            'from': previous_month_day_start,
            'to': previous_month_day_end
        }
        date_range.insert(0, obj)

        current_month_day_start = previous_month_day_start
    return date_range

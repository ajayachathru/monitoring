import logging
import smtplib
import ssl
from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from src.utils.config import MAILER_CONFIG


def send_mail(mail):
    # From local config
    sender_email = MAILER_CONFIG['sender_email']
    password = MAILER_CONFIG['pwd']

    if 'receiver_email' in mail:
        receiver_email = mail['receiver_email']
    else:
        receiver_email = MAILER_CONFIG['receiver_email']

    # Create a multipart message and set headers
    message = MIMEMultipart('alternative')
    message["From"] = sender_email
    message["To"] = ', '.join(receiver_email)
    message["Subject"] = mail['subject']

    # Add html / plain body to email
    if 'html_template_file' in mail:
        # Open html template file
        with open(mail['html_template_file'], "r") as template:
            file_content = template.read()
            if 'html_body_params' in mail:
                # Interpolate params if necessary
                file_content = file_content.format(**mail['html_body_params'])
            message.attach(MIMEText(file_content, 'html'))

    elif 'body' in mail:
        message.attach(MIMEText(mail['body'], 'plain'))

    if 'filepath' in mail:
        # Open PDF file in binary mode
        with open(mail['filepath'], "rb") as attachment:
            # Add file as application/octet-stream
            # Email client can usually download this automatically as attachment
            part = MIMEBase("application", "octet-stream")
            part.set_payload(attachment.read())

            # Encode file in ASCII characters to send by email
            encoders.encode_base64(part)

            # Add header as key/value pair to attachment part
            part.add_header(
                "Content-Disposition",
                f"attachment; filename= {mail['filename']}",
            )

            # Add attachment to message and convert message to string
            message.attach(part)

    try:
        with smtplib.SMTP(host=MAILER_CONFIG['host'], port=MAILER_CONFIG['port']) as server:
            server.ehlo()
            server.starttls()
            server.login(sender_email, password)
            server.sendmail(sender_email, receiver_email, message.as_string())

            logging.info("Email triggered : " + message["Subject"])
    except Exception as err:
        logging.error("Error connecting mail server\t" + str(err))

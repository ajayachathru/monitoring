import logging
from src.models.netnodes_model import NetNodesModel


class NetNodesService:

    def __init__(self):
        pass

    def get_net_nodes(self, pipeline=[]):
        logging.info('Fetching net nodes ...')
        return NetNodesModel.objects().aggregate(pipeline, allowDiskUse=True)



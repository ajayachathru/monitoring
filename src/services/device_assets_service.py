import logging
from src.models.device_assets_model import DeviceAssetsModel


class DeviceAssetsService:

    def __init__(self):
        pass

    def get_assets(self, pipeline=[]):
        logging.info('Fetching device assets ...')
        return DeviceAssetsModel.objects().aggregate(pipeline, allowDiskUse=True)



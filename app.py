import logging
import logging.handlers
import sys
from mongoengine import connect, disconnect

from src.general_jobs.check_email_password_expiry import SystemMail
from src.reports.events_amr_consumption_report import AmrEventsReport
from src.reports.itron_consumption_report import ItronConsumptionReport
from src.reports.rtu_health_status_report import RtuStatData
from src.temetra_scraper import TemetraScraper
from src.utils.config import DB_CONFIG


class App:
    db_connection_established = False

    def __init__(self):
        logging.info("App class instance created")

    def connect_db(self):
        try:
            # connect db using mongo engine
            connect(DB_CONFIG['name'], host=DB_CONFIG['uri'])

            self.db_connection_established = True

        except Exception as e:
            # TODO: Handle specific exception
            logging.error("Couldn't establish DB connection", e)

    def disconnect_db(self):
        disconnect(DB_CONFIG['name'])
        self.db_connection_established = False


if __name__ == "__main__":
    if len(sys.argv) > 1:
        """
            Run directly from command line..
            passing job_id as a command line arg to execute reports        
            1. Daily RTU Health status report
            2. Monthly AMR consumption report             
            3. Monthly Itron consumption report
            4. Temetra Data Processing
            5. Check email password expiry and trigger email to admin to reset password. Need update pwdExpiryDate
            field in config.py after resetting password  
        """
        job_id = sys.argv[1]
        logging.basicConfig(
            level=logging.INFO,
            format="%(asctime)s\t%(levelname)s\t%(module)s\t%(funcName)s\t%(message)s",
            datefmt="%Y-%m-%d\t%H:%M:%S",
            handlers=[logging.StreamHandler(),
                      logging.handlers.TimedRotatingFileHandler("logs/logger_" + str(job_id) + ".txt", when="midnight",
                                                                interval=1)]
        )
        logging.info("Job id to execute\t" + job_id)

        app_instance = App()
        app_instance.connect_db()

        if app_instance.db_connection_established:
            if job_id == '1':
                logging.info('Running job 1 **********')
                rtu_instance = RtuStatData()
                rtu_instance.daily_rtu_health_monitoring()

            elif job_id == '2':
                logging.info('Running job 2 **********')
                amr_report_instance = AmrEventsReport()
                amr_report_instance.monthly_amr_consumption_report()

            elif job_id == '3':
                logging.info('Running job 3 **********')
                itron_report_instance = ItronConsumptionReport()
                itron_report_instance.monthly_itron_consumption_report(sys.argv[2])

            elif job_id == '4':
                logging.info('Running job 4 **********')
                temetra_scraper_instance = TemetraScraper()
                temetra_scraper_instance.run(sys.argv[2])

            elif job_id == '5':
                logging.info('Running job 5 **********')
                system_mail_instance = SystemMail()
                system_mail_instance.check_password_expiry()

            else:
                logging.error("**** Invalid arg ****")
        else:
            logging.error("**** Couldn't connect to DB ****")

    else:
        logging.error("**** Job id not found in arg ****")

#!/bin/bash
echo "Running docker container in ${ENVIRONMENT} environment";

# Create static folders required by app
/bin/mkdir -p outputs;
/bin/mkdir -p logs;

#Replace config with environment specific file
cp environments/${ENVIRONMENT,,}_env.py src/utils/config.py;

/usr/bin/crontab -u root scheduler.txt;
/usr/sbin/service cron start;